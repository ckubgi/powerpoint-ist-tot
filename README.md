# PowerPoint ist tot!
In diesem Repositorium finden Sie eine Präsentation auf Basis von Markdown+[Pandoc][pandoc]+[Reveal.js][reveal]+[GitLabCI][gitlabci]. Sie zeigt exemplarisch wie mit Hilfe dieser drei Werkzeuge flexible, versionierbare und optisch ansprechende Präsentationen erstellt werden können.
Sie basiert auf "Benji's Slide Decks by [Benji Fisher][slide-decks]".

Diese Präsentation ist ein Lightning-Talk für den [Forschungsdatentag 2021][FD-Tag], einer Veranstaltung zum Thema Forschungsdaten im Rahmen des [Digitaltags][Digi-Tag] am 18.06.2021.

[slide-decks]:https://gitlab.com/benjifisher/slide-decks
[FD-Tag]:https://www.uni-marburg.de/de/hefdi/veranstaltungen/forschungsdatentag-2021
[Digi-Tag]:https://digitaltag.eu/
[pandoc]:https://pandoc.org
[reveal]:https://revealjs.com/
[gitlabci]:https://docs.gitlab.com/ee/ci/README.html

## Demo

Die in diesem Projekt erzeugte Präsentation finden Sie unter
[GitLab Pages](https://ckubgi.gitlab.io/powerpoint-ist-tot).



<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
<img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" />
</a>
<br />
<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">PowerPoint ist tot!</span> by
<a xmlns:cc="http://creativecommons.org/ns#" href="https://ahrdey.gitlab.io/powerpoint-ist-tot" property="cc:attributionName" rel="cc:attributionURL">Christian Krippes</a>
is licensed under a
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
<br />Based on a work at
<a xmlns:dct="http://purl.org/dc/terms/" href="https://gitlab.com/benjifisher/slide-decks" rel="dct:source">https://gitlab.com/benjifisher/slide-decks</a>.
