---
author: Christian Krippes
title: 'PowerPoint ist tot!'
subtitle: 'Vorträge mit Markdown, Pandoc, Reveal.js und GitLabCI'
date: 18.06.2021
revealjs-url: 'reveal.js'
mathjax: 'true'
mathjaxurl: 'MathJax/es5/tex-chtml-full.js'
theme: custom-simple
---

# Markdown

## Markdown: Listen

```md
1. Erstens
2. Zweitens
    1. Erster Unterpunkt
    2. Zweiter Unterpunkt
```

1. Erstens
2. Zweitens
    1. Erster Unterpunkt
    2. Zweiter Unterpunkt

## Markdown: Links

```md
[JLU](https://www.uni-giessen.de)
```

[JLU](https://www.uni-giessen.de)

## Markdown: Bilder

```md
![PowerPoint Symbolbild](./images/Bird.svg)
```

![PowerPoint - Symbolbild](./images/Bird.svg){ width=25% height=25%}

## Markdown: Überschriften

```md
### H3 Überschrift

#### H4 Überschrift

##### H5 Überschrift

###### H6 Überschrift
```

### H3 Überschrift

#### H4 Überschrift

##### H5 Überschrift

###### H6 Überschrift

# Pandoc

## Pandoc: a universal document converter

- Schweizer Taschenmesser für Markup Konvertierung
- Method: anything -> AST -> anything
- [https://pandoc.org/](https://pandoc.org/)

## Pandoc: Input-Formate

<div style="color: #42affa">
```sh
$ pandoc --list-input-formats
biblatex
bibtex
commonmark
commonmark_x
creole
csljson
csv
docbook
docx
dokuwiki
epub
fb2
gfm
haddock
html
ipynb
jats
jira
json
latex
man
markdown
markdown_github
markdown_mmd
markdown_phpextra
markdown_strict
mediawiki
muse
native
odt
opml
org
rst
t2t
textile
tikiwiki
twiki
vimwiki
```
</div>

## Pandoc: Output-Formate

<div style="color: #42affa">
```sh
$ pandoc --list-output-formats
asciidoc
asciidoctor
beamer
biblatex
bibtex
commonmark
commonmark_x
context
csljson
docbook
docbook4
docbook5
docx
dokuwiki
dzslides
epub
epub2
epub3
fb2
gfm
haddock
html
html4
html5
icml
ipynb
jats
jats_archiving
jats_articleauthoring
jats_publishing
jira
json
latex
man
markdown
markdown_github
markdown_mmd
markdown_phpextra
markdown_strict
mediawiki
ms
muse
native
odt
opendocument
opml
org
pdf
plain
pptx
revealjs
rst
rtf
s5
slideous
slidy
tei
texinfo
textile
xwiki
zimwiki
```
</div>

## Pandoc: Nutzung

<div style="color: #42affa">
```sh
$ pandoc \
 --standalone \
 -t revealjs \
 -o html/devops-slides.html \
 markdown/devops-slides.md
```
</div>

# Reveal.js

## Reveal.js: The HTML Presentation Framework

- Home page: [https://revealjs.com/#/](https://revealjs.com/#/)
- GitHub repo: [https://github.com/hakimel/reveal.js](https://github.com/hakimel/reveal.js)

# Formeln und Graphen

## Formeln: MathJax

$$ {\displaystyle \operatorname {Var} (X):=\mathbb {E} \left((X-\mu )^{2}\right)=\int _{\Omega }(X-\mu )^{2}\,\mathrm {d} P.} $$

## Graphen mit plot.ly
<div class="container">
   <iframe class="responsive-iframe" scrolling="no" src="iris-plot.html"></iframe>
</div>

## Graphen mit plot.ly
```python
import plotly
import os
import pandas as pd
import plotly.express as px

def main():
    # df = px.data.iris()
    df = pd.read_csv('data/iris.csv')
    fig = px.scatter(df, x="sepal_width", y="sepal_length", color="species",
                     size='petal_length', hover_data=['petal_width'])
    plotly.offline.plot(fig, filename="html/iris-plot.html", image_width=1080, image_height=1920, auto_open=False)

if __name__ == "__main__":
    main()
```
# GitLab CI

## GitLab CI: Übersicht

- voll in GitLab integriert
- frei für öffentliche und private Repositorien
- führt Jobs auf beliebigen Docker Images aus
- Runner kann auch lokal laufen

## GitLab CI: Pages

- Neuen Job `pages` anlegen.
- Dateien zu `public/` hinzufügen.
- Als Artefakt speichern.

## GitLab CI: Beispiel

Datei: `.gitlab-ci.yml`

```yaml
variables:
  GIT_SUBMODULE_STRATEGY: recursive

image:
  name: pandoc/ubuntu-latex:2.11.4  
  entrypoint: [""]


before_script:
  - apt-get update -qq && apt-get install -qq -y pandoc python3-pip
  - pandoc --version
  - pip install pandas plotly

pages:
  stage: deploy
  only:
    - main
  script:
    - make build
    - ls html
    - cp -R html public
    - ls public
  artifacts:
    paths:
      - public
```

# Pro und Contra

## Pro

- Offene Technik (Webstandards)
- Trennung von Inhalt und Darstellung (Markdown->HTML+CSS+JS)
- Skalierbarkeit für div. Endgeräte
- Publikumsinteraktion (Tickets, Merge-Requests)
- Automatisierung von Aufgaben möglich (bspw. Graphen)

## Contra

- Durchaus steile Lernkurve
- Mehr Möglichkeiten für Fehler
- Ohne GitLab "nur" lokal
- Einbindung von CDN-Quellen erschwert Datenschutz

# Demo

## Der Doktorrand hat neue Daten

- Merge-Request mit Update von `data/iris.csv`
- merge
- run pipeline (make)
