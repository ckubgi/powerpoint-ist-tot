#!/usr/bin/env python3

import plotly
import os
import pandas as pd
import plotly.express as px

def main():
    # df = px.data.iris()
    df = pd.read_csv('data/iris.csv')
    fig = px.scatter(df, x="sepal_width", y="sepal_length", color="species",
                     size='petal_length', hover_data=['petal_width'])
    plotly.offline.plot(fig, filename="html/iris-plot.html", image_width=1080, image_height=1920, auto_open=False)

if __name__ == "__main__":
    main()
